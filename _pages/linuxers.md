---
layout: page
title: Mailing list
subtitle: Linuxers mailing list
desc: About Linuxers mailing list
permalink: /linuxers/
---

<div class="pretty-links">
We use Linuxers mailing list to discuss and keep in touch. You can subscribe to the list and catch up with the community or read the [archives &rarr;](http://mm.ilug-bom.org.in/pipermail/linuxers)

---

### Subscribe via Mailman interface

Visit [Linuxers listinfo](http://mm.ilug-bom.org.in/mailman/listinfo/linuxers) page in your web browser, scroll down to the section titled *Subscribing to Linuxers* and fill up the form.

Mailman will process your request and send you a confirmation email. Simply click the confirmation link in that email to complete your subscription.


### Subscribe via email

Send an empty email to *linuxers-request@mm.ilug-bom.org.in* with **subscribe** in the subject line.

Mailman will process your request and send you a confirmation email. Simply reply to that email with its subject line intac to complete your subscription.

</div>

