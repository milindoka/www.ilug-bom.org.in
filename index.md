---
layout: page
---

<div class="lead lead-about pretty-links">
Upcoming: [August 2017 Meetup](/2017/august-2017-meetup).  
Past Meetup Update: [July 2017 Meetup at D.B.I.T.](/2017/july-2017-meetup)
</div>
---
### Welcome to ILUG-BOM!
The GNU/Linux Users Group of Mumbai, India, also called ILUG-BOM, is an
informal mailing list of over 1200 members from all over India, but
mostly from Mumbai.

We discuss issues related to promoting, supporting the GNU/Linux OS,
various distributions, sharing free/open source resources, have
informal and formal meetings, seminars and organise workshops now and then. [More &rarr;](/events)

We also help schools and institutions in migrating to free/libre and opensource based solutioins. To contact us for migration related queries please drop us a mail at *ilug.mumbai@gmail.com*
